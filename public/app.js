'use strict';

async function addUserFetch(userInfo) {
  let options = {
    method: "POST",
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify({ name: userInfo.name, lastname: userInfo.lastname })
  }

  let response = await fetch("/users", options)
  if (response.status === 201) {
    let user = await response.json();
    return user.id;
  } else {
    throw new Error(`${response.status} ${response.statusText}`);
  }
}

async function getUserFetch(id) {
  let options = {
    method: "GET",
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  };

  let response = await fetch(`/users/${id}`, options);
  if (response.status === 200) {
    return response.json();
  } else {
    throw new Error(`${response.status} ${response.statusText}`);
  }
}


function addUserXHR(userInfo) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/users");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.responseType = 'json';
    xhr.send(JSON.stringify({ name: userInfo.name, lastname: userInfo.lastname }));
    xhr.onload = function () {
      if (xhr.status !== 201) {
        reject(new Error(`${xhr.status} ${xhr.statusText}`));
      } else {
        resolve(xhr.response)
      }
    };
  });
}

function getUserXHR(id) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", `/users/${id}`);
    xhr.responseType = 'json';
    xhr.send();
    xhr.onload = function () {
      if (xhr.status !== 200) {
        reject(new Error(`${xhr.status} ${xhr.statusText}`));
      } else {
        resolve(xhr.response)
      }
    };
  });
}


function checkWork() {
  addUserXHR({ name: "Malice", lastname: "XHR" })
    .then(response => {
      console.log(`Был добавлен пользователь с userId ${response.id}`);
      return getUserXHR(response.id);
    })
    .then(response => {
      console.log(`Был получен пользователь:`, response.id);
    })
    .catch(error => {
      console.error(error);
    });
}
checkWork();